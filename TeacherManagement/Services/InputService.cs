namespace TeacherManagement.Services;

public static class InputService
{
    public static int GetInt(string msg, int min, int max)
    {
        int result = -1;
        string txt = null;
        do
        {
            Console.WriteLine(msg);
            try
            {
                txt = Console.ReadLine();
                result = int.Parse(txt);
                if (result <= min || result > max) throw new Exception();
               

            }
            catch (Exception e)
            {
                txt = null;
            }
            
        } while (txt == null);

        return result;
    }
    
    public static double GetDouble(string msg, double min, double max)
    {
        double result = -1;
        string txt = null;
        do
        {
            Console.WriteLine(msg);
            try
            {
                txt = Console.ReadLine();
                result = int.Parse(txt);
                if (result <= min || result > max) throw new Exception();
                

            }
            catch (Exception e)
            {
                txt = null;
            }
            
        } while (txt == null);

        return result;
    }
    
    public static string getString(string msg)
    {
        string txt = null;
        do
        {
            Console.WriteLine(msg);
            try
            {
                txt = Console.ReadLine();
            }
            catch (Exception e)
            {
                txt = null;
            }
            
        } while (txt == null);

        return txt;
    }
}