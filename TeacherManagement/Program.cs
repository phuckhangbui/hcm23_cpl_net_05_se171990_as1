﻿// See https://aka.ms/new-console-template for more information

using TeacherManagement.Entity;
using TeacherManagement.Services;

int numberOfMember = InputService.GetInt("Nhap so Giao vien: ", 0, 10);

if (numberOfMember <= 0)
{
    throw new Exception("So giao vien phai lon hon 0");
}

List<GiaoVien> list = new List<GiaoVien>();
double minLuong = 0;
int indexMin = -1;

for (int i = 0; i < numberOfMember; i++)
{
    string hoTen = InputService.getString("Nhap ho va ten Giao Vien " + i + 1);

    int namSinh = InputService.GetInt("Nhap nam sinh: ", 1920, 2002);

    double luongCoBan = InputService.GetDouble("Nhap luong co ban: ", 0, double.MaxValue);
    
    double heSoLuong = InputService.GetDouble("Nhap he so luong: ", 0, double.MaxValue);

    GiaoVien gv = new GiaoVien();
    gv.NhapThongTin(hoTen, namSinh, luongCoBan);
    gv.NhapThongTin(heSoLuong);
    
    list.Add(gv);

    if (minLuong < gv.TinhLuong()) minLuong = gv.TinhLuong();

    indexMin = i;
}

Console.WriteLine("Giao vien co luong thap nhap la: ");
GiaoVien minGv = list[indexMin];

minGv.XuatThongTinh();

Console.ReadLine();
