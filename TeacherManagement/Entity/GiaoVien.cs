namespace TeacherManagement.Entity;

public class GiaoVien: NguoiLaoDong
{
    public double HeSoLuong {
        get;
        set;
    }

    public GiaoVien()
    {
    }
    

    public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
    {
        HeSoLuong = heSoLuong;
    }

    public void NhapThongTin(double heSoLuong)
    {
        HeSoLuong = heSoLuong;
    }

    public double TinhLuong()
    {
        return HeSoLuong * LuongCoBan * 1.25;
    }

    public double XuLy()
    {
        return HeSoLuong + 0.6;
    }

    public new void XuatThongTinh()
    {
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, " +
                          $"luong co ban: {LuongCoBan}, he so luong: {HeSoLuong}, " +
                          $"luong: {TinhLuong()}");
    }

    void XuatThongTin()
    {
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, " +
                          $"luong co ban: {LuongCoBan}, he so luong: {HeSoLuong}, " +
                          $"luong: {TinhLuong()}");
    }
}