namespace TeacherManagement.Entity;

public class NguoiLaoDong
{
    public string HoTen { get; set; }
    public int NamSinh { get; set; }
    public double LuongCoBan { get; set; }

    public NguoiLaoDong()
    {
    }

    public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }
    

    public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public double TinhLuong()
    {
        return this.LuongCoBan;
    }

    public void XuatThongTinh()
    {
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}");
    }
}